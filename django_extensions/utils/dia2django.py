    # -*- coding: UTF-8 -*-
##Author Igor Támara igor@tamarapatino.org
##Use this little program as you wish, if you
#include it in your work, let others know you
#are using it preserving this note, you have
#the right to make derivative works, Use it
#at your own risk.
#Tested to work on(etch testing 13-08-2007):
#  Python 2.4.4 (#2, Jul 17 2007, 11:56:54)
#  [GCC 4.1.3 20070629 (prerelease) (Debian 4.1.2-13)] on linux2

import codecs
import gzip
import re
import sys
from pprint import pprint
import xml.etree.ElementTree as ET
from xml.dom.minidom import *  # NOQA
from inflection import dasherize, underscore
import os
import errno

import six

dependclasses = ["User", "Group", "Permission", "Message"]
excludeclasses = ["AuthUser", "GameApplication"]

#Type dictionary translation types SQL -> Django
tsd = {
    "text": "TextField",
    "date": "DateField",
    "varchar": "CharField",
    "int": "IntegerField",
    "integer": "IntegerField",
    "float": "FloatField",
    "serial": "AutoField",
    "boolean": "BooleanField",
    "numeric": "FloatField",
    "timestamp": "DateTimeField",
    "bigint": "IntegerField",
    "datetime": "DateTimeField",
    "date": "DateField",
    "time": "TimeField",
    "bool": "BooleanField",
    "int": "IntegerField",
    "nullbool": "NullBooleanField",
    "image" : "ImageField",
    "tinyint" : "IntegerField",
}

#convert varchar -> CharField
v2c = re.compile('varchar\((\d+)\)')
uuid = "id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)"

def index(fks, id):
    """Looks for the id on fks, fks is an array of arrays, each array has on [1]
    the id of the class in a dia diagram.  When not present returns None, else
    it returns the position of the class with id on fks"""
    for i, j in fks.items():
        if fks[i][1] == id:
            return i
    return None


def addparentstofks(rels, fks):
    """Gets a list of relations, between parents and sons and a dict of
    clases named in dia, and modifies the fks to add the parent as fk to get
    order on the output of classes and replaces the base class of the son, to
    put the class parent name.
    """
    for j in rels:
        son = index(fks, j[1])
        parent = index(fks, j[0])

        if parent == None:
            continue;

        try:
            fks[son][2] = fks[son][2].replace("models.Model", parent)
            if parent not in fks[son][0]:
                fks[son][0].append(parent)
        except Exception, e:
            print ("error = addparentstofks(%s, %s)") % (son,parent)
            raise e



def adduuid(clases):
    for j, k in six.iteritems(clases):
        str = clases[j][2]
        # clases[j][2] = str.replace("    #replacement\n", "")
        if str.find("models.Model") == -1:
            clases[j][2] = str.replace("    #replacement\n", "")
        else:
            clases[j][2] = str.replace("#replacement", uuid)


def addassoctofks(rels, fks):
    """Gets a list of relations, between parents and sons and a dict of
    clases named in dia, and modifies the fks to add the parent as fk to get
    order on the output of classes and replaces the base class of the son, to
    put the class parent name.
    """
    # pprint(fks)
    for j in rels:
        parent = index(fks, j[1])
        dep_name = index(fks, j[0])

        relation = dep_name.lower() + " = models.ForeignKey(" + dep_name + ")\n    relation"
        fks[parent][2] = fks[parent][2].replace("relation", relation)
        # if parent not in fks[son][0]:
        #     fks[son][0].append(parent)

def cleanupoutput(clases):
    for j in clases:
        clases[j][2] = clases[j][2].replace("    relation\n", "")

def make_sure_path_exists(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

def dia2file(archivo):
    f = codecs.open(archivo, "rb")
    #dia files are gzipped
    data = gzip.GzipFile(fileobj=f).read()
    return data

def dia2django(archivo, outdir):
    models_txt = ''
    f = codecs.open(archivo, "rb")
    #dia files are gzipped
    data = gzip.GzipFile(fileobj=f).read()
    ppal = parseString(data)
    #diagram -> layer -> object -> UML - Class -> name, (attribs : composite -> name,type)
    datos = ppal.getElementsByTagName("dia:diagram")[0].getElementsByTagName("dia:layer")[0].getElementsByTagName("dia:object")
    clases = {}
    herit = []
    assoc = []


    imports = six.u("")
    for i in datos:
        #Look for the classes
        if i.getAttribute("type") == "UML - Class":
            myid = i.getAttribute("id")
            class_meta = {
                "classname" : "",
                "inherit" : "",
                "fields" : {}
            }

            for j in i.childNodes:
                if j.nodeType == Node.ELEMENT_NODE and j.hasAttributes():
                    first_field = ""

                    if j.getAttribute("name") == "name":
                        actclas = j.getElementsByTagName("dia:string")[0].childNodes[0].data[1:-1]

                        if (actclas in excludeclasses):
                            break;

                        print "Generate: %s" % actclas;
                        myname = "\nclass %s(models.Model) :\n    #replacement\n" % (actclas)

                        # clases[actclas] = [[], myid, myname, 0, "", ClassMeta(actclas)]
                        class_meta["classname"] = actclas
                        clases[actclas] = [[], myid, myname, 0, "", class_meta]
                        # print "m2: " + mycomment

                    if j.getAttribute("name") == "comment":
                        mycomment = j.getElementsByTagName("dia:string")[0].childNodes[0].data[5:-1]
                        if len(mycomment) > 0:
                            clases[actclas][4] = mycomment + " or u''"

                    if j.getAttribute("name") == "attributes":
                        for l in j.getElementsByTagName("dia:composite"):
                            if l.getAttribute("type") == "umlattribute":
                                #Look for the attribute name and type
                                comment = ""
                                meta_field = {
                                    "name" : "",
                                    "type" : "",
                                    "toString": "",
                                }
                                for k in l.getElementsByTagName("dia:attribute"):
                                    if k.getAttribute("name") == "name":
                                        nc = k.getElementsByTagName("dia:string")[0].childNodes[0].data[1:-1]
                                        meta_field['name'] = nc

                                    elif k.getAttribute("name") == "comment":
                                        list = k.getElementsByTagName("dia:string")[0].childNodes[0].data[1:-1]
                                        if val == '##':
                                            val = ''
                                        else:
                                            list = list.split("\n")
                                            if list[0].startswith("#"):
                                                comment = list[0]
                                                del list[0]
                                            val = ', '.join(map(str,list))

                                    elif k.getAttribute("name") == "type":
                                        tc = k.getElementsByTagName("dia:string")[0].childNodes[0].data[1:-1]
                                        meta_field['type'] = tc

                                        if tc.lower().startswith("fk") or tc.lower().startswith("mm") \
                                            or tc.lower().startswith("on") or tc.lower().startswith("en"):
                                            relation_name = tc[3:-1]
                                            # if dependclasses.count(relation_name) == 0:
                                                    # dependclasses.append(relation_name)
                                            continue

                                    elif k.getAttribute("name") == "value":
                                        val = k.getElementsByTagName("dia:string")[0].childNodes[0].data[1:-1]
                                        if val == '##':
                                            val = ''
                                    elif k.getAttribute("name") == "visibility" and k.getElementsByTagName("dia:enum")[0].getAttribute("val") == "2":

                                        if tc.replace(" ", "").lower().startswith("manytomanyfield("):
                                                #If we find a class not in our model that is marked as being to another model
                                                newc = tc.replace(" ", "")[16:-1]
                                                if dependclasses.count(newc) == 0:
                                                        dependclasses.append(newc)
                                        if tc.replace(" ", "").lower().startswith("foreignkey("):
                                                #If we find a class not in our model that is marked as being to another model
                                                newc = tc.replace(" ", "")[11:-1]
                                                if dependclasses.count(newc) == 0:
                                                        dependclasses.append(newc)


                                #Mapping SQL types to Django
                                varch = v2c.search(tc)
                                if tc.replace(" ", "").startswith("ManyToManyField("):
                                    myfor = tc.replace(" ", "")[16:-1]
                                    if actclas == myfor:
                                        #In case of a recursive type, we use 'self'
                                        tc = tc.replace(myfor, "'self'")
                                    elif clases[actclas][0].count(myfor) == 0:
                                        #Adding related class
                                        if myfor not in dependclasses:
                                            #In case we are using Auth classes or external via protected dia visibility
                                            clases[actclas][0].append(myfor)
                                    tc = "models." + tc
                                    if len(val) > 0:
                                        tc = tc.replace(")", "," + val + ")")
                                elif tc.find("Field") != -1:
                                    if tc.count("()") > 0 and len(val) > 0:
                                        tc = "models.%s" % tc.replace(")", "," + val + ", null=True, blank=True)")
                                    else:
                                        tc = "models.%s(%s, %s)" % (tc, val, "null=True, blank=True")
                                elif tc.replace(" ", "").startswith("ForeignKey("):
                                    myfor = tc.replace(" ", "")[11:-1]
                                    if actclas == myfor:
                                        #In case of a recursive type, we use 'self'
                                        tc = tc.replace(myfor, "'self'")
                                    elif clases[actclas][0].count(myfor) == 0:
                                        #Adding foreign classes
                                        if myfor not in dependclasses:
                                            #In case we are using Auth classes
                                            clases[actclas][0].append(myfor)
                                    tc = "models." + tc
                                    if len(val) > 0:
                                        tc = tc.replace(")", "," + val + ")")
                                elif varch is None:
                                    if tc.lower().startswith("fk"):
                                        tc = "models.ForeignKey" + "(" + relation_name + ")"
                                        if len(val) == 0:
                                            tc = tc.replace(")", ", null=True, blank=True)")
                                        else:
                                            tc = tc.replace(")", ", null=True, blank=True, "+ val + ")")

                                        #Adding foreign classes
                                        if relation_name not in dependclasses:
                                            #In case we are using Auth classes
                                            clases[actclas][0].append(relation_name)
                                    elif tc.lower().startswith("mm"):
                                        tc = "models.ManyToManyField" + "(" + relation_name + ")"
                                        if relation_name not in dependclasses:
                                            #In case we are using Auth classes
                                            clases[actclas][0].append(relation_name)
                                    elif tc.lower().startswith("on"): #one2one
                                        tc = "models.OneToOneField" + "(" + relation_name + ")"

                                        if relation_name not in dependclasses:
                                            #In case we are using Auth classes
                                            clases[actclas][0].append(relation_name)

                                    elif tc.lower().startswith("en"): #one2one
                                        tc = "enum.EnumField" + "(" + relation_name + ")"
                                    else:
                                        try:
                                            tc = "models." + tsd[tc.strip().lower()] + "(" + val + ")"
                                            if val.find('primary_key') == -1:
                                                if len(val) == 0:
                                                    tc = tc.replace(")", "null=True, blank=True)")
                                                else:
                                                    tc = tc.replace(")", ", null=True, blank=True)")
                                        except Exception, e:
                                            print ("error = entity: %s, type: %s ") % (clases[actclas][5]['classname'], tc.strip().lower())
                                            raise e
                                else:

                                    tc = "models.CharField(max_length=" + varch.group(1) + ")"
                                    if len(val) > 0:
                                        tc = tc.replace(")", ", " + val + ")")

                                    if val.find('primary_key') == -1:
                                        tc = tc.replace(")", ", null=True, blank=True)")


                                if len(comment) > 0:
                                    tc = tc + " " + comment

                                if not (nc == "id" and tc == "AutoField()"):
                                    clases[actclas][2] = clases[actclas][2] + ("    %s = %s\n" % (nc, tc))
                                    if len(first_field) == 0 and tc.find("models.ForeignKey") == -1:
                                        first_field = nc

                                class_meta['fields'][meta_field['name']] = meta_field

                                # clases[actclas][5].add_field(meta_field)


                        if len(clases[actclas][4]) == 0:
                            # clases[actclas][4] = "unicode(self." + first_field + ") or u''"
                            if len(first_field) == 0:
                                clases[actclas][4] = "u''"
                            else:
                                clases[actclas][4] = "self." + first_field + " or u''"





        elif i.getAttribute("type") == "UML - Generalization":
            mycons = ['A', 'A']
            a = i.getElementsByTagName("dia:connection")
            for j in a:
                if len(j.getAttribute("to")):
                    mycons[int(j.getAttribute("handle"))] = j.getAttribute("to")
            if 'A' not in mycons:
                herit.append(mycons)
        elif i.getAttribute("type") == "UML - SmallPackage":
            a = i.getElementsByTagName("dia:string")
            for j in a:
                if len(j.childNodes[0].data[1:-1]):
                    imports += six.u("from %s.models import *" % j.childNodes[0].data[1:-1])
        elif i.getAttribute("type") == "UML - Association":
            mycons = ['A', 'A']
            related_name = ''
            for k in i.getElementsByTagName("dia:attribute"):
                if k.getAttribute("name") == "name":
                    relation_name = k.getElementsByTagName("dia:string")[0].childNodes[0].data[1:-1]

            a = i.getElementsByTagName("dia:connection")
            for j in a:
                if len(j.getAttribute("to")):
                    mycons[int(j.getAttribute("handle"))] = j.getAttribute("to")

            if 'A' not in mycons:
                assoc.append(mycons)

    addparentstofks(herit, clases)
    # adduuid(clases)
    # addassoctofks(assoc, clases)

    # print "My class"
    # pprint(clases)
    # cleanupoutput(clases)

    #Ordering the appearance of classes
    #First we make a list of the classes each classs is related to.
    ordered = []
    # pprint(dependclasses)
    for j, k in six.iteritems(clases):
        if len(k[4]) > 5 :
            k[2] = k[2] + "\n    def %s(self):\n        return %s\n" % (("__str__" if six.PY3 else "__unicode__"), k[4] ,)
        else:
            k[2] = k[2] + "\n    def %s(self):\n        return u\"\"\n" % (("__str__" if six.PY3 else "__unicode__"), )

        for fk in k[0]:
            try:
                if fk not in dependclasses:
                    clases[fk][3] += 1
            except Exception, e:
                print ("error: dependclasses (%s, %s)") % (j, fk)
                raise e

        ordered.append([j] + k)

    i = 0
    while i < len(ordered):
        mark = i
        j = i + 1
        while j < len(ordered):
            if ordered[i][0] in ordered[j][1]:
                mark = j
            j += 1
        if mark == i:
            i += 1
        else:
            # swap %s in %s" % ( ordered[i] , ordered[mark]) to make ordered[i] to be at the end
            if ordered[i][0] in ordered[mark][1] and ordered[mark][0] in ordered[i][1]:
                #Resolving simplistic circular ForeignKeys
                print("Not able to resolve circular ForeignKeys between %s and %s" % (ordered[i][1], ordered[mark][0]))
                break
            a = ordered[i]
            ordered[i] = ordered[mark]
            ordered[mark] = a
        if i == len(ordered) - 1:
            break
    ordered.reverse()

    if imports:
        models_txt = str(imports)
    for i in ordered:
        models_txt += '%s\n' % str(i[3])

    #prepare output dir
    outdir += "/.generate"
    make_sure_path_exists(outdir)


    file_headers = [
        'from __future__ import unicode_literals',
        '',
        'import uuid',
        'from django.db import models',
        'from django.contrib.auth.models import User',
        '',
        'from rest_framework import routers, serializers, viewsets, generics',
        'from .models_enum import *'
        '',
        ''
    ]
    _header = "\n".join(file_headers)

    with open(outdir + "/models.py", "w") as text_file:
        text_file.write(_header)
        text_file.write(models_txt)



    #make serializer
    serializers_txt = ""
    views_text = ""
    admin_text = ""
    for j, k in six.iteritems(clases):
        obj = clases[j]
        meta = obj[5]
        fields = []
        classname = meta['classname']
        for fname in meta['fields']:
            fields.append(fname)

        _field_str = "', '".join(fields)

        adminstr = ("""
@admin.register(models.%s)
class %sAdmin (admin.ModelAdmin):
    #list_display = ('%s')
    pass
""") % (classname, classname, _field_str)
        admin_text += "%s\n" % adminstr

        sstr = ("""
class %sSerializer(serializers.ModelSerializer):
    class Meta:
        model = %s
        depth = 1
        #fields = ('%s')
""") % (classname, classname, _field_str)
        serializers_txt += "%s\n" % sstr

        viewstr = ("""
class %sViewSet(viewsets.ModelViewSet):
    queryset = %s.objects.all()
    serializer_class = %sSerializer
    filter_fields = ('%s', )

""") % (classname, classname, classname, _field_str)

        views_text += "%s\n" % viewstr


    file_headers = [
        'from django.contrib import admin',
        'import models',
        ''
    ]
    _header = "\n".join(file_headers)

    with open(outdir + "/_admin.py", "w") as text_file:
        text_file.write(_header)
        text_file.write(admin_text)

    file_headers = [
        'from from django.contrib import admin',
        'import models',
        '',
        ''
    ]
    _header = "\n".join(file_headers)

    with open(outdir + "/admin.py", "w") as text_file:
        text_file.write(_header)
        text_file.write(admin_text)

    file_headers = [
        'from __future__ import unicode_literals',
        'from rest_framework import routers, serializers, viewsets, generics',
        'from rest_framework.validators import UniqueValidator, UniqueTogetherValidator',
        'from drf_extra_fields.fields import Base64ImageField',
        'from .models import *',
        '',
        ''
    ]
    _header = "\n".join(file_headers)

    with open(outdir + "/serializers.py", "w") as text_file:
        text_file.write(_header)
        text_file.write(serializers_txt)
        models_txt += sstr


    file_headers = [
        'from __future__ import unicode_literals',
        'from rest_framework import routers, serializers, viewsets, generics',
        'from rest_framework.validators import UniqueValidator, UniqueTogetherValidator',
        'from drf_extra_fields.fields import Base64ImageField',
        'from .models import *',
        'from .serializers import *',
        '',
        ''
    ]
    _header = "\n".join(file_headers)
    with open(outdir + "/views.py", "w") as text_file:
        text_file.write(_header)
        text_file.write(views_text)
        models_txt += sstr

    #make api_urls
    file_headers = [
        'from django.conf.urls import patterns, url, include',
        'from rest_framework.routers import DefaultRouter',
        'from .views import *',
        ''
    ]
    _header = "\n".join(file_headers)

    route_txt = ""
    for j, k in six.iteritems(clases):
        obj = clases[j]
        meta = obj[5]
        classname = meta['classname']

        url = dasherize(underscore(classname))


        route_txt += "router.register(r'%s', %sViewSet, '%s')\n" % (url, classname, url)

    body = """

router = DefaultRouter()
%s
urlpatterns = patterns('',
<<<<<<< HEAD
    url(r'', include(router.urls)),
=======
    url(r'/', include(router.urls)),
>>>>>>> 62e8069dbcdd91c29286b755f555ed5830805488
)
    """ % route_txt



    with open(outdir + "/api_urls.py", "w") as text_file:
        text_file.write(_header)
        text_file.write(body)


    file_headers = [
        'from __future__ import unicode_literals',
        '',
        'from django.db import models',
        'from django.contrib.auth.models import User',
        '',
        'from rest_framework import routers, serializers, viewsets, generics',
        'from .models_enum import *',
        'from ._genius import *'
    ]
    _header = "\n".join(file_headers)

    return _header + "\n" + models_txt

if __name__ == '__main__':
    if len(sys.argv) == 3:
        dia2django(sys.argv[1], sys.argv[2])
    else:
        print(" Use:\n \n   " + sys.argv[0] + " diagram.dia output_dir\n\n")